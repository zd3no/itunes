package rest;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import model.Playlist;
import model.Track;
import model.User;

import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import service.PlaylistService;
import service.TrackService;
import service.UserService;

/**
 * This class is used in parsing the XML and persisting it to DB
 * @author zdeno
 */
@Path("/parse")
public class Parser {
	
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private int lastTrackId = 0;
	private Map<Integer, Integer> trackIdMap = new HashMap<>();
	private Map<Integer, Track> newTracksMap = new HashMap<>();
	private Collection<Playlist> newPlaylists = new ArrayList<>();
	private Map<String, Track> userTracks = new HashMap<>();
	private Map<Integer, Track> userTracksMappedByID = new HashMap<>();
	private User thisUser = null;
	private Node trackNode = null;
	
	@EJB UserService userService;
	@EJB TrackService trackService;
	@EJB PlaylistService playlistService;

	/**
	 * The method used to upload the xml file from client to database. Verifies if the user is valid and parses the input stream 
	 * into tracks and playlists that are persisted into DB. Each user has a persistent library ID which must match the library id of the
	 * file to be parsed
	 * @param input - MultipartFormDataInput of the file/files to be parsed
	 * @param headers - HTTP headers that must contain "user" header with user details
	 * @return Response containing current user info or error message as String
	 * @throws UnsupportedEncodingException 
	 */
	@Path("/uploadFile")
	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response parseFile(MultipartFormDataInput input, @Context HttpHeaders headers, @Context HttpServletRequest request) throws UnsupportedEncodingException {
		request.setCharacterEncoding("UTF-8");
		if (headers != null) {
			List<String> userHeader = headers.getRequestHeader("user");
			if (userHeader == null || userHeader.isEmpty())
				return Response.serverError()
						.entity("Null user in the header!").build();
			else {
				String userID = userHeader.get(0);
				thisUser = userService.getUserById(userID);
				if (thisUser == null) {
					Response.serverError()
					.entity("No such user in the database.").build();
				}
			}
		}
		Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
		List<InputPart> inputParts = uploadForm.get("uploadedFile");
		for (InputPart inputPart : inputParts) {
			try {
				InputStream inputStream = inputPart.getBody(InputStream.class,
						null);
				Reader reader = new InputStreamReader(inputStream,"UTF-8");
				InputSource is = new InputSource(reader);
				is.setEncoding("UTF-8");
				
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory
						.newInstance();
				dbFactory.setIgnoringComments(true);
				dbFactory.setIgnoringElementContentWhitespace(true);
				dbFactory.setNamespaceAware(true);
				dbFactory.setValidating(true);
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(is);
				doc.getDocumentElement().normalize();
				
				Element root = doc.getDocumentElement();
				Node firstDict = root.getFirstChild();
				Node nextNode = firstDict.getFirstChild();
				Node trackRootDict = nextNode.getNextSibling();
				if(!isLibraryIdSame(thisUser.getLiraryId(), trackRootDict))
					return Response.serverError().entity("Invalid library id").build();
				long start = System.currentTimeMillis();
				for (Track track : trackService.getExistingTracksForUser(thisUser)) {
					userTracks.put(track.getPersistentId(), track);
					userTracksMappedByID.put(track.getTrackId(), track);
				}
				lastTrackId = trackService.getLastTrackId();
				System.out.println("INIT STAGE: "+(System.currentTimeMillis()-start));
				start = System.currentTimeMillis();
				extractTracks(trackRootDict);
				System.out.println("CREATE TRACKS STAGE: "+(System.currentTimeMillis()-start));
				start = System.currentTimeMillis();
				for (Track xtrack : newTracksMap.values()) {
					if (xtrack.getName().equalsIgnoreCase("Trip to Your Heart")) {
						System.out.println(xtrack.toString());
					}
				}
				trackService.persist(newTracksMap.values());
				System.out.println("PERSIST TRACKS STAGE: "+(System.currentTimeMillis()-start));
				start = System.currentTimeMillis();
				extractPlaylists(trackNode);
				System.out.println("CREATE PLAYLISTS STAGE: "+(System.currentTimeMillis()-start));
				start = System.currentTimeMillis();
				playlistService.update(newPlaylists);
				System.out.println("PERSIST PLAYLISTS STAGE: "+(System.currentTimeMillis()-start));
			} catch (Exception e) {
				e.printStackTrace();
				return Response.serverError()
						.entity("Server error: " + e.getMessage()).build();
			}
		}
		return Response.ok().entity(thisUser).build();
	}

	/**
	 * Verifies that the library ID of the user matches the library ID of the file to be parsed
	 * @param libraryId - user library ID
	 * @param firstDict - the root dict of the document 
	 * @return true or false
	 */
	private boolean isLibraryIdSame(String libraryId, Node firstDict) {
		Node nextNode = firstDict;
		int i=0;
		while(i < 1000){
			i++;
			if(nextNode.hasChildNodes()){
				if (nextNode.getFirstChild().getNodeValue().equals("Library Persistent ID")) {
					if (libraryId == null) {
						thisUser.setLiraryId(nextNode.getNextSibling().getFirstChild().getNodeValue());
						userService.merge(thisUser);
						return true;
					}
					if (nextNode.getNextSibling().getFirstChild().getNodeValue().equals(libraryId))
						return true;
					else
						return false;
				}
			}
			nextNode = nextNode.getNextSibling();
		}
		return false;
	}

	/**
	 * Extracts the playlist nodes from the document
	 * @param trackRootDict
	 */
	private void extractPlaylists(Node trackRootDict) {
		//trackRootDict is the trackNode which is the dict element.
		//next sibling is <key>Playlists</key> and the one after that is the <array>
		Element playlistRootArrayElement = (Element) trackRootDict.getNextSibling().getNextSibling(); //array

		Node playlistDict = playlistRootArrayElement.getFirstChild();
		Node lastChild = playlistRootArrayElement.getLastChild();
		do{
			if (playlistDict.getNodeType() == Node.ELEMENT_NODE) {
				createPlaylist((Element) playlistDict); // dict
			}
			playlistDict = playlistDict.getNextSibling();
		}while(playlistDict!=lastChild);
	}

	/**
	 * Generates a playlist based on the provided playlist node
	 * @param playlistDict
	 */
	private void createPlaylist(Element playlistDict) {
		Playlist playlist = new Playlist();
		NodeList playlistAttributes = playlistDict.getElementsByTagName("key");
		playlist.setName(extractStringKeyValue("Name", playlistAttributes));
		playlist.setPersistentId(extractStringKeyValue("Playlist Persistent ID", playlistAttributes));
		playlist.setUser(thisUser);
		addPlaylistTracks(playlist, playlistDict);
		newPlaylists.add(playlist);
	}

	/**
	 * Each playlist has many tracks. This method adds those tracks to playlist. Tracks must already exist
	 * @param playlist - which playlist will be filled
	 * @param playlistDict - the node of the playlist which is then used to extract track nodes
	 */
	private void addPlaylistTracks(Playlist playlist, Element playlistDict) {
		NodeList arraysNodes = playlistDict.getElementsByTagName("array");
		if (arraysNodes.getLength() == 0) {
			return;
		}
		Element array = (Element)arraysNodes.item(0);
		NodeList tracks = array.getElementsByTagName("key");
		for (int i = 0; i < tracks.getLength(); i++) {
			Node next = tracks.item(i);
			if (next.getFirstChild().getNodeValue().equalsIgnoreCase("Track ID")) {
				int indexId = Integer.valueOf(next.getNextSibling().getFirstChild().getNodeValue());
				int myTrackIndex = trackIdMap.get(indexId);
				Track track = newTracksMap.get(myTrackIndex); //get Track from the new tracks we've read in from file
				if (track == null) { //New tracks are skipped if track is already in the user DB Track table
					track = userTracksMappedByID.get(myTrackIndex); //get the Track from the existing user tracks instead
				}
				playlist.addTrack(track);
			}
		}
	}

	/**
	 * Extracts tracks from the document based on the track root dict provided
	 * @param trackRootDict
	 */
	private void extractTracks(Node trackRootDict) {
		while(true){
			if(trackRootDict.hasChildNodes()){
				if (trackRootDict.getFirstChild().getNodeValue().equals("Tracks")) {
					break;
				}
			}
			trackRootDict = trackRootDict.getNextSibling();
		}
		Element trackRootElement = (Element) trackRootDict.getNextSibling();
		trackNode = trackRootElement;

		NodeList tracks = trackRootElement.getElementsByTagName("dict");
		for (int i = 0; i < tracks.getLength(); i++) {
			createTrack((Element) tracks.item(i));
		}
	}

	/**
	 * Create the track object based on the trackDict provided
	 */
	private void createTrack(Element trackDict) {
		Track track = new Track();
		NodeList trackAttributes = trackDict.getElementsByTagName("key");
		int trackId = (int) extractNumericKeyValue("Track ID", trackAttributes);
		track.setPersistentId(extractStringKeyValue("Persistent ID", trackAttributes));
		if (userTracks.containsKey(track.getPersistentId())){ //check if the same track is already in the DB for this user
			trackIdMap.put(trackId, userTracks.get(track.getPersistentId()).getTrackId());
			return;
		}
		lastTrackId++;
		track.setTrackId(lastTrackId);
		track.setName(extractStringKeyValue("Name", trackAttributes));
		track.setArtist(extractStringKeyValue("Artist", trackAttributes));
		track.setComposer(extractStringKeyValue("Composer", trackAttributes));
		track.setAlbum(extractStringKeyValue("Album", trackAttributes));
		track.setGenre(extractStringKeyValue("Genre", trackAttributes));
		track.setKind(extractStringKeyValue("Kind", trackAttributes));
		track.setSize(extractNumericKeyValue("Size", trackAttributes));
		track.setTotalTime(extractNumericKeyValue("Total Time", trackAttributes));
		track.setDateModified(extractDateKeyValue("Date Modified", trackAttributes));
		track.setDateAdded(extractDateKeyValue("Date Added", trackAttributes));
		track.setBitRate((int) extractNumericKeyValue("Bit Rate", trackAttributes));
		track.setSampleRate((int) extractNumericKeyValue("Sample Rate", trackAttributes));
		track.setPlayCount((int) extractNumericKeyValue("Play Count", trackAttributes));
		track.setPlayDate(new Date(extractNumericKeyValue("Play Date", trackAttributes)));
		track.setHasVideo(extractBooleanKeyValue("Has Video", trackAttributes));
		track.setHd(extractBooleanKeyValue("HD", trackAttributes));
		track.setVideoHeight((int) extractNumericKeyValue("Video Height", trackAttributes));
		track.setVideoWidth((int) extractNumericKeyValue("Video Width", trackAttributes));
		track.setUser(thisUser);
		trackIdMap.put(trackId, lastTrackId);
		newTracksMap.put(lastTrackId, track);
	}

	/** 
	 * Extract the value of the String value from node named "key" from the list of nodes provided "trackAttributes"
	 * @param key - name of the node to extract value from
	 * @param trackAttributes - The keys node list of a single track
	 * @return String
	 */
	private String extractStringKeyValue(String key, NodeList trackAttributes) {
		Node next = null;
		for (int i = 0; i < trackAttributes.getLength(); i++) {
			next = trackAttributes.item(i);
			if (next.getFirstChild().getNodeValue().equalsIgnoreCase(key)) {
				return next.getNextSibling().getFirstChild().getNodeValue();
			}
		}
		return null;
	}
	
	/** 
	 * Extract the value of the Long value from node named "key" from the list of nodes provided "trackAttributes"
	 * @param key - name of the node to extract value from
	 * @param trackAttributes - The keys node list of a single track
	 * @return Long
	 */
	private long extractNumericKeyValue(String key, NodeList trackAttributes) {
		Node next = null;
		for (int i = 0; i < trackAttributes.getLength(); i++) {
			next = trackAttributes.item(i);
			if (next.getFirstChild().getNodeValue().equalsIgnoreCase(key)) {
				return Long.valueOf(next.getNextSibling().getFirstChild().getNodeValue());
			}
		}
		return 0;
	}
	
	/** 
	 * Extract the value of the Date value from node named "key" from the list of nodes provided "trackAttributes"
	 * @param key - name of the node to extract value from
	 * @param trackAttributes - The keys node list of a single track
	 * @return Date
	 */
	private Date extractDateKeyValue(String key, NodeList trackAttributes) {
		Node next = null;
		for (int i = 0; i < trackAttributes.getLength(); i++) {
			next = trackAttributes.item(i);
			if (next.getFirstChild().getNodeValue().equalsIgnoreCase(key)) {
				try {
					return sdf.parse(next.getNextSibling().getFirstChild().getNodeValue().replace("T", " ").replace("Z", ""));
				} catch (DOMException | ParseException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}
	
	/** 
	 * Extract the value of the boolean value from node named "key" from the list of nodes provided "trackAttributes"
	 * @param key - name of the node to extract value from
	 * @param trackAttributes - The keys node list of a single track
	 * @return true or false	
	 */
	private boolean extractBooleanKeyValue(String key, NodeList trackAttributes) {
		Node next = null;
		for (int i = 0; i < trackAttributes.getLength(); i++) {
			next = trackAttributes.item(i);
			if (next.getFirstChild().getNodeValue().equalsIgnoreCase(key)) {
				return next.getNextSibling().getNodeName().equalsIgnoreCase("true") ? true : false;
			}
		}
		return false;
	}
	
}

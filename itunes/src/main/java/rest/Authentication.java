package rest;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import model.User;
import service.UserService;

@Stateless
@Path("/authenticate")
public class Authentication {
	
	@EJB private UserService userService;

	@GET
	@Path("/login")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response login(@QueryParam("username") String username, @QueryParam("password") String password) {
		User user = userService.getUserById(username);
		if(user!=null) {
			if(user.getPassword().equals(password)) 
				return Response.ok().entity(user).build();
		}
		return Response.ok().status(401).entity("Not authorized").build();
	}
	
	@POST
	@Path("/register")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response register(@FormParam("userId")String userId, @FormParam("name")String name, @FormParam("password")String password) {
		if (name==null || name.equals("") || !name.matches("^[a-zA-Z\\s\\']+$")
				|| password==null || password.equals("") || password.length()<6
				|| userId==null || userId.equals("") || userId.length()<6) {
			return Response.ok().status(404).entity("Server: Invalid details!").build();
		}
		User user = userService.getUserById(userId);
		if(user==null) {
			user = new User();
			user.setName(name);
			user.setPassword(password);
			user.setUserId(userId);
			userService.persist(user);
			return Response.ok().entity(user).build();
		}
		return Response.ok().status(404).entity("User already registered").build();
	}
}

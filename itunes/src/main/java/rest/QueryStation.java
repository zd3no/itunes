package rest;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import model.Playlist;
import model.Track;
import model.User;
import service.PlaylistService;
import service.TrackService;
import service.UserService;

/**
 * This class is used in all queries from the client
 * @author zdeno
 *
 */
@Path("/query")
public class QueryStation {
	
	@EJB private UserService userService;
	@EJB private TrackService trackService;
	@EJB private PlaylistService playlistService;
	private User thisUser = null;
	
	/**
	 * Get all Tracks which do not have a video. This is not a user playlist but a concatenation of all tracks from DB
	 * @param headers - HTTP header expected to contain current user details
	 * @return
	 */
	@Path("/getAllAudioFiles")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllAudioFiles(@Context HttpHeaders headers){
		if (headers != null) {
			List<String> userHeader = headers.getRequestHeader("user");
			if (userHeader == null || userHeader.isEmpty())
				return Response.serverError()
						.entity("Null user in the header!").build();
			else {
				String userID = userHeader.get(0);
				thisUser = userService.getUserById(userID);
				if (thisUser == null) {
					Response.serverError()
					.entity("No such user in the database.").build();
				}
			}
		}
		return Response.ok().entity(trackService.getMusic(thisUser)).build();
	}
	
	/**
	 * Return a list of playlists from the DB for current user
	 * @param headers - HTTP header expected to contain current user details
	 * @return
	 */
	@Path("/getAllPlaylists")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllPlaylists(@Context HttpHeaders headers){
		if (headers != null) {
			List<String> userHeader = headers.getRequestHeader("user");
			if (userHeader == null || userHeader.isEmpty())
				return Response.serverError()
						.entity("Null user in the header!").build();
			else {
				String userID = userHeader.get(0);
				thisUser = userService.getUserById(userID);
				if (thisUser == null) {
					Response.serverError()
					.entity("No such user in the database.").build();
				}
			}
		}
		return Response.ok().entity(playlistService.getPlaylistNamesForUser(thisUser)).build();
	}
	
	/**
	 * Get all Tracks which have a video. This is not a user playlist but a concatenation of all tracks from DB
	 * @param headers - HTTP header expected to contain current user details
	 * @return
	 */
	@Path("/getAllVideoFiles")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllVideoFiles(@Context HttpHeaders headers){
		if (headers != null) {
			List<String> userHeader = headers.getRequestHeader("user");
			if (userHeader == null || userHeader.isEmpty())
				return Response.serverError()
						.entity("Null user in the header!").build();
			else {
				String userID = userHeader.get(0);
				thisUser = userService.getUserById(userID);
				if (thisUser == null) {
					Response.serverError()
					.entity("No such user in the database.").build();
				}
			}
		}
		return Response.ok().entity(trackService.getVideo(thisUser)).build();
	}
	
	/**
	 * Return all tracks for the current playlist
	 * @param headers - HTTP header expected to contain current user details
	 * @param persistentId - playlist persistent ID
	 * @return
	 */
	@Path("/getTracksForPlaylist")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTracksForPlaylist(@Context HttpHeaders headers, @QueryParam("persistentId") String persistentId){
		if (headers != null) {
			List<String> userHeader = headers.getRequestHeader("user");
			if (userHeader == null || userHeader.isEmpty())
				return Response.serverError()
						.entity("Null user in the header!").build();
			else {
				String userID = userHeader.get(0);
				thisUser = userService.getUserById(userID);
				if (thisUser == null) {
					Response.serverError()
					.entity("No such user in the database.").build();
				}
			}
		}
		if(persistentId == null || persistentId.isEmpty())
			return Response.serverError()
					.entity("Cannot find such playlist!").build();
		return Response.ok().entity(playlistService.getMyTracks(thisUser, persistentId)).build();
	}
	
	/**
	 * Get all playlists for the track
	 * @param userId
	 * @param id
	 * @return
	 */
	@Path("/getPlaylistsOfTrack")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPlaylistsOfTrack(@QueryParam("user")String userId ,@QueryParam("id")Integer id){
		thisUser = userService.getUserById(userId);
		if (thisUser == null) {
			Response.serverError()
			.entity("No such user in the database.").build();
		}
		return Response.ok().entity(playlistService.getPlaylistsContainingTrack(id, thisUser)).build();
	}
	
	/**
	 * Edit the track
	 * @param id- track id
	 * @param name - new name
	 * @param artist - new artist
	 * @param album -new album
	 * @param genre -new genre
	 * @param composer - new composer
	 * @return
	 */
	@Path("/editTrack")
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	public Response editTrack(@FormParam("id")Integer id, @FormParam("name")String name, 
			@FormParam("artist")String artist, @FormParam("album")String album, 
			@FormParam("genre")String genre, @FormParam("composer")String composer){
		Track track = trackService.getTrackById(id);
		track.setName(name);
		track.setArtist(artist);
		track.setAlbum(album);
		track.setGenre(genre);
		track.setComposer(composer);
		track.setDateModified(new Date());
		if(trackService.update(track))
			return Response.ok().status(200).entity(System.currentTimeMillis()).build();
		else
			return Response.ok().status(404).entity("Couldn't update").build();
	}
	
	/**
	 * Delete the track fir the provided ID
	 * @param id
	 * @return
	 */
	@Path("/deleteTrack")
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	public Response deleteTrack(@FormParam("id")Integer id){
		if(trackService.delete(id))
			return Response.ok().status(200).entity("OK").build();
		else
			return Response.ok().status(404).entity("Couldn't delete").build();
	}
	
	/**
	 * Delete playlist with provided ID
	 * @param playlistId
	 * @return
	 */
	@Path("/deletePlaylist")
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	public Response deletePlaylist(@FormParam("playlistId")String playlistId){
		Playlist playlist = playlistService.getPlaylistById(playlistId);
		if(playlistService.delete(playlist))
			return Response.ok().status(200).entity("OK").build();
		else
			return Response.ok().status(500).entity("Couldn't delete").build();
	}
	
	/**
	 * Remove the track with provided trackID from Playlist
	 * @param id - track ID
	 * @param playlistID - playlist ID
	 * @return
	 */
	@Path("/removeTrackFromPlaylistOnly")
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	public Response removeTrackFromPlaylistOnly(@FormParam("id")Integer id, @FormParam("playlistId")String playlistID){
		if(playlistService.removeTrackFromPlaylistOnly(id, playlistID))
			return Response.ok().status(200).entity("OK").build();
		else
			return Response.ok().status(404).entity("Couldn't remove").build();
	}
	
	/**
	 * Append track to the provided playlist
	 * @param trackId
	 * @param playlistId
	 * @return
	 */
	@Path("/addTrackToThisPlaylist")
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	public Response addTrackToThisPlaylist(@FormParam("trackId")Integer trackId, @FormParam("playlistId")String playlistId){
		if(playlistService.addTrackToPlaylist(trackId, playlistId))
			return Response.ok().status(200).entity("OK").build();
		else
			return Response.ok().status(404).entity("Couldn't remove").build();
	}
	
	/**
	 * Update user details
	 * @param name
	 * @param oldUserId
	 * @param newUserId
	 * @param password
	 * @return
	 */
	@Path("/updateUser")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response addTrackToThisPlaylist(@FormParam("name")String name, @FormParam("oldUserId")String oldUserId,@FormParam("newUserId")String newUserId, @FormParam("password")String password){
		User user = userService.getUserById(oldUserId);
		user.setName(name);
		user.setPassword(password);
		user.setUserId(newUserId);
		if(userService.update(oldUserId, user))
			return Response.ok().status(200).entity(user).build();
		else
			return Response.ok().status(404).entity("Couldn't Update user").build();
	}
	
	/**
	 * Create a new playlist
	 * @param headers - HTTP headers that contain the user
	 * @param name - playlist Name
	 * @return
	 */
	@Path("/createPlaylist")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response createPlaylist(@Context HttpHeaders headers, @QueryParam("playlistName")String name){
		if (headers != null) {
			List<String> userHeader = headers.getRequestHeader("user");
			if (userHeader == null || userHeader.isEmpty())
				return Response.serverError()
						.entity("Null user in the header!").build();
			else {
				String userID = userHeader.get(0);
				thisUser = userService.getUserById(userID);
				if (thisUser == null) {
					Response.serverError()
					.entity("No such user in the database.").build();
				}
			}
		}
		Playlist playlist = new Playlist();
		playlist.setName(name);
		playlist.setPersistentId(Long.toHexString(System.nanoTime()));
		playlist.setUser(thisUser);
		playlistService.persist(playlist);
		return Response.ok().entity(playlist).build();
	}

	/**
	 * Edit playlist name
	 * @param headers - HTTP headers that contain the user
	 * @param name - new name
	 * @param playlistId 
	 * @return
	 */
	@Path("/editPlaylistName")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response performEditActionOnPlaylist(@Context HttpHeaders headers, @QueryParam("playlistName")String name, 
			@QueryParam("playlistId")String playlistId){
		if (headers != null) {
			List<String> userHeader = headers.getRequestHeader("user");
			if (userHeader == null || userHeader.isEmpty())
				return Response.serverError()
						.entity("Null user in the header!").build();
			else {
				String userID = userHeader.get(0);
				thisUser = userService.getUserById(userID);
				if (thisUser == null) {
					Response.serverError()
					.entity("No such user in the database.").build();
				}
			}
		}
		Playlist playlist = playlistService.getPlaylistById(playlistId);
		playlist.setName(name);
		playlistService.update(playlist);
		return Response.ok().entity(playlist).build();
	}
}

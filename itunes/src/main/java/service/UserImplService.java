package service;

import java.util.Collection;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;

import model.User;
import dao.UserDAO;

@Local
@Stateless
public class UserImplService implements UserService {
	@EJB UserDAO userDAO;

	public UserImplService() {}

	@Override
	public boolean persist(User user) {
		return userDAO.persist(user);
	}

	@Override
	public boolean update(String oldUserId, User user) {
		return userDAO.update(oldUserId, user);
	}

	@Override
	public boolean delete(User user) {
		return userDAO.delete(user);
	}

	@Override
	public Collection<User> getUsers() {
		return userDAO.getUsers();
	}

	@Override
	public User getUserById(String id) {
		return userDAO.getUserById(id);
	}

	@Override
	public void merge(User thisUser) {
		userDAO.merge(thisUser);
	}

}

package service;

import java.util.Collection;

import javax.ejb.Local;

import model.User;

@Local
public interface UserService {

	boolean persist(User user);
	boolean update(String oldUserId, User user);
	boolean delete(User user);
	Collection<User>getUsers();
	User getUserById(String id);
	void merge(User thisUser);
}

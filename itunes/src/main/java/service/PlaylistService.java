package service;

import java.util.Collection;
import java.util.Map;

import javax.ejb.Local;

import model.Playlist;
import model.Track;
import model.User;

@Local
public interface PlaylistService {
	Collection<Playlist> getPlaylists();
	Map<String, Playlist> getPlaylistsForUser(User user);
	Playlist getPlaylistById(String persistentId);
	boolean persist(Playlist playlist);
	boolean update(Playlist playlist);
	void update(Collection<Playlist> playlists);
	boolean delete(Playlist playlist);
	Collection<String> getPlaylistsContainingTrack(Integer trackId, User user);
	Collection<String[]> getPlaylistNamesForUser(User user);
	Collection<Track> getMyTracks(User thisUser, String persistentId);
	boolean remove(Track track);
	boolean removeTrackFromPlaylistOnly(Integer trackId, String playlistID);
	boolean addTrackToPlaylist(Integer trackId, String playlistId);
}

package service;

import java.util.Collection;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;

import model.Track;
import model.User;
import dao.TrackDAO;

@Local
@Stateless
public class TrackImplService implements TrackService {
	
	@EJB TrackDAO trackDAO;

	@Override
	public Collection<Track> getTracks() {
		return trackDAO.getTracks();
	}

	@Override
	public Track getTrackById(int id) {
		return trackDAO.getTrackById(id);
	}

	@Override
	public boolean persist(Track track) {
		return trackDAO.persist(track);
	}

	@Override
	public boolean update(Track track) {
		return trackDAO.update(track);
	}

	@Override
	public boolean delete(Integer trackid) {
		return trackDAO.delete(trackid);
	}

	@Override
	public Collection<Track> getExistingTracksForUser(User user) {
		return trackDAO.getExistingTracksForUser(user);
	}

	@Override
	public int getLastTrackId() {
		return trackDAO.getLastTrackId();
	}

	@Override
	public void persist(Collection<Track> tracks) {
		trackDAO.persist(tracks);
	}

	@Override
	public Collection<Object[]> getMusic(User user) {
		return trackDAO.getMusic(user);
	}

	@Override
	public Collection<Object[]> getVideo(User thisUser) {
		return trackDAO.getVideo(thisUser);
	}

}

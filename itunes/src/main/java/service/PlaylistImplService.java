package service;

import java.util.Collection;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;

import model.Playlist;
import model.Track;
import model.User;
import dao.PlaylistDAO;

@Local
@Stateless
public class PlaylistImplService implements PlaylistService {
	
	@EJB PlaylistDAO playlistDAO;

	@Override
	public Collection<Playlist> getPlaylists() {
		return playlistDAO.getPlaylists();
	}

	@Override
	public Playlist getPlaylistById(String persistentId) {
		return playlistDAO.getPlaylistById(persistentId);
	}

	@Override
	public boolean persist(Playlist playlist) {
		return playlistDAO.persist(playlist);
	}

	@Override
	public boolean update(Playlist playlist) {
		return playlistDAO.update(playlist);
	}

	@Override
	public boolean delete(Playlist playlist) {
		return playlistDAO.delete(playlist);
	}

	@Override
	public Map<String, Playlist> getPlaylistsForUser(User user) {
		return playlistDAO.getPlaylistsForUser(user);
	}

	@Override
	public void update(Collection<Playlist> playlists) {
		playlistDAO.update(playlists);
	}

	@Override
	public Collection<String> getPlaylistsContainingTrack(Integer trackId, User user) {
		return playlistDAO.getPlaylistsContainingTrack(trackId, user);
	}

	@Override
	public Collection<String[]> getPlaylistNamesForUser(User user) {
		return playlistDAO.getPlaylistNamesForUser(user);
	}

	@Override
	public Collection<Track> getMyTracks(User thisUser, String persistentId) {
		return playlistDAO.getMyTracks(thisUser, persistentId);
	}

	@Override
	public boolean remove(Track track) {
		return playlistDAO.remove(track);
	}

	@Override
	public boolean removeTrackFromPlaylistOnly(Integer trackId,
			String playlistID) {
		return playlistDAO.removeTrackFromPlaylistOnly(trackId, playlistID);
	}

	@Override
	public boolean addTrackToPlaylist(Integer trackId, String playlistId) {
		return playlistDAO.addTrackToPlaylist(trackId, playlistId);
	}

}

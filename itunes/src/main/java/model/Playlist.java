package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity @Table(name="playlists", indexes= {@Index(columnList="persistent_id")})
public class Playlist implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id @Column(name="persistentId")
	private String persistentId;
	private String name;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="playlist", fetch=FetchType.EAGER)
	private Collection<PlaylistTrack> tracks;
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private User user;
	
	public Playlist() {
		tracks = new ArrayList<PlaylistTrack>();
	}

	public String getPersistentId() {
		return persistentId;
	}

	public void setPersistentId(String persistentId) {
		this.persistentId = persistentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Collection<Track> getTracks() {
		Collection<Track> list= new ArrayList<>();
		for (PlaylistTrack playlistTrack : tracks) {
			list.add(playlistTrack.getTrack());
		}
		return list;
	}
	
	public void addTrack(Track track) {
//		Collection<Track> list= new ArrayList<>();
//		for (PlaylistTrack playlistTrack : tracks) {
//			list.add(playlistTrack.getTrack());
//		}
//		if (!list.contains(track)) {
			PlaylistTrack p = new PlaylistTrack();
			p.setPlaylist(this);
			p.setTrack(track);
			tracks.add(p);
//        }
	}
	
	public void removeTrack(PlaylistTrack track) {
		if(tracks.contains(track)) {
			tracks.remove(track);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((persistentId == null) ? 0 : persistentId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Playlist other = (Playlist) obj;
		if (persistentId == null) {
			if (other.persistentId != null)
				return false;
		} else if (!persistentId.equals(other.persistentId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Playlist [persistentId=" + persistentId + ", name=" + name
				+ ", tracks=" + tracks + ", user=" + user + "]";
	}
	
}

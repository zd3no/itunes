package model;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Singleton
@Startup
public class MySequencer {
	
	private static int lastplaylistTrackId =0;

	@PersistenceContext
	EntityManager em;
	
	@PostConstruct
	private void init(){
//		Collection<Integer> list = em.createQuery("SELECT pl.id FROM PlaylistTrack pl ORDER BY pl.id DESC",Integer.class).setMaxResults(1).getResultList();
//		if(list.size()>0)
//			lastplaylistTrackId = list.iterator().next();
//		System.out.println("SIZE IS: "+lastplaylistTrackId);
	}
	
	public static int getNextplaylistTrackId() {
		lastplaylistTrackId = lastplaylistTrackId+1;;
		return lastplaylistTrackId;
	}
}

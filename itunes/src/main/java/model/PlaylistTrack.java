package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="playlists_tracks")
public class PlaylistTrack implements Serializable{
	
	private static final long serialVersionUID = 1L;

//	@SuppressWarnings("static-access")
	public PlaylistTrack(){
//		this.id = new MySequencer().getNextplaylistTrackId();
	}
	
	@Transient
	private static Integer nextID;
	
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid")
	@Column(columnDefinition = "CHAR(32)")
	@Id
	private String id;
	
	@ManyToOne
    private Playlist playlist;
	
	@OneToOne
	private Track track;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Playlist getPlaylist() {
		return playlist;
	}

	public void setPlaylist(Playlist playlist) {
		this.playlist = playlist;
	}

	public Track getTrack() {
		return track;
	}

	public void setTrack(Track track) {
		this.track = track;
	}
	
	
}

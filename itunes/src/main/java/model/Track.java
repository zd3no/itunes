package model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="tracks", indexes={@Index(columnList="persistentId")})
public class Track implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id @Column(name="track_id")
	private int trackId;
	private String name;
	private String artist;
	private String album;
	private String genre;
	private String kind;
	private String composer;
	private long size;
	@Column(name="total_time")
	private long totalTime;
	@Column(name="date_added")
	private Date dateAdded;
	@Column(name="date_modfied")
	private Date dateModified;
	@Column(name="bit_rate")
	private Integer bitRate;
	@Column(name="sample_rate")
	private Integer sampleRate;
	@Column(name="persistent_id") 
	private String persistentId;
	@Column(name="play_count")
	private int playCount;
	@Column(name="play_date")
	private Date playDate;
	@Column(name="has_video")
	private boolean hasVideo = false;
	private boolean hd = false;
	@Column(name="video_width")
	private Integer videoWidth;
	@Column(name="video_height")
	private Integer videoHeight;
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private User user;
	
	public Track() {
	}

	public int getTrackId() {
		return trackId;
	}

	public void setTrackId(int trackId) {
		this.trackId = trackId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public String getAlbum() {
		return album;
	}

	public void setAlbum(String album) {
		this.album = album;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public long getTotalTime() {
		return totalTime;
	}

	public void setTotalTime(long totalTime) {
		this.totalTime = totalTime;
	}

	public Date getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}

	public Date getDateModified() {
		return dateModified;
	}

	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}

	public Integer getBitRate() {
		return bitRate;
	}

	public void setBitRate(Integer bitRate) {
		this.bitRate = bitRate;
	}

	public Integer getSampleRate() {
		return sampleRate;
	}

	public void setSampleRate(Integer sampleRate) {
		this.sampleRate = sampleRate;
	}

	public String getPersistentId() {
		return persistentId;
	}

	public void setPersistentId(String persistentId) {
		this.persistentId = persistentId;
	}

	public int getPlayCount() {
		return playCount;
	}

	public void setPlayCount(int playCount) {
		this.playCount = playCount;
	}

	public Date getPlayDate() {
		return playDate;
	}

	public void setPlayDate(Date playDate) {
		this.playDate = playDate;
	}

	public boolean isHasVideo() {
		return hasVideo;
	}

	public void setHasVideo(boolean hasVideo) {
		this.hasVideo = hasVideo;
	}

	public boolean isHd() {
		return hd;
	}

	public void setHd(boolean hd) {
		this.hd = hd;
	}

	public Integer getVideoWidth() {
		return videoWidth;
	}

	public void setVideoWidth(Integer videoWidth) {
		this.videoWidth = videoWidth;
	}

	public Integer getVideoHeight() {
		return videoHeight;
	}

	public void setVideoHeight(Integer videoHeight) {
		this.videoHeight = videoHeight;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getComposer() {
		return composer;
	}

	public void setComposer(String composer) {
		this.composer = composer;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((persistentId == null) ? 0 : persistentId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Track other = (Track) obj;
		if (persistentId == null) {
			if (other.persistentId != null)
				return false;
		} else if (!persistentId.equals(other.persistentId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Track [trackId=" + trackId + ", name=" + name + ", artist="
				+ artist + ", album=" + album + ", genre=" + genre + ", kind="
				+ kind + ", composer=" + composer + ", size=" + size
				+ ", totalTime=" + totalTime + ", dateAdded=" + dateAdded
				+ ", dateModified=" + dateModified + ", bitRate=" + bitRate
				+ ", sampleRate=" + sampleRate + ", persistentId="
				+ persistentId + ", playCount=" + playCount + ", playDate="
				+ playDate + ", hasVideo=" + hasVideo + ", hd=" + hd
				+ ", videoWidth=" + videoWidth + ", videoHeight=" + videoHeight
				+ ", user=" + user+"]";
	}

	
}

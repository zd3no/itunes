package dao;

import java.util.Collection;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.Playlist;
import model.Track;
import model.User;

@Local
@Stateless
public class UserImplDAO implements UserDAO {
	@PersistenceContext
	private EntityManager em;

	public UserImplDAO() {
	}

	@Override
	public boolean persist(User user) {
		if (user == null)
			return false;
		try {
			em.persist(user);
			return true;
		}catch(EntityExistsException e) {
			return false;
		}
	}

	@Override
	public boolean update(String oldUserId, User newUser) {
		if (newUser == null || oldUserId == null)
			return false;
		User oldUser = em.find(User.class, oldUserId);
		if (oldUser == null) {
			return false;
		}
		if(oldUserId.equalsIgnoreCase(newUser.getUserId())){
			newUser = em.merge(newUser);
			return true;
		}else{
			newUser.setLiraryId(oldUser.getLiraryId());
			em.persist(newUser);
			Collection<Playlist> list = em.createQuery("SELECT p FROM Playlist p where p.user = :oldUser",Playlist.class).setParameter("oldUser", oldUser).getResultList();
			for (Playlist playlist : list) {
				playlist.setUser(newUser);
				em.merge(playlist);
			}
			Collection<Track> trackList = em.createQuery("SELECT t FROM Track t where t.user = :oldUser",Track.class).setParameter("oldUser", oldUser).getResultList();
			for (Track track : trackList) {
				track.setUser(newUser);
				em.merge(track);
			}
			em.remove(oldUser);
			return true;
		}
	}

	@Override
	public boolean delete(User user) {
		if (user == null)
			return false;
		User existingUser = getUserById(user.getUserId());
		if (existingUser.equals(user)) {
			em.remove(existingUser);
			return true;
		}
		return false;
	}

	@Override
	public Collection<User> getUsers() {
		return em.createQuery("Select u From User u", User.class).getResultList();
	}

	@Override
	public User getUserById(String id) {
		Collection<User> users = em.createQuery("Select u from User u Where u.userId = :userId",User.class).setParameter("userId", id).getResultList();
		return users.size() > 0 ? users.iterator().next() : null;
	}

	@Override
	public void merge(User thisUser) {
		if (thisUser!=null) {
			em.merge(thisUser);
		}
	}

}

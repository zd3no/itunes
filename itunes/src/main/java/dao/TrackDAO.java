package dao;

import java.util.Collection;

import javax.ejb.Local;

import model.Track;
import model.User;

@Local
public interface TrackDAO {
	
	Collection<Track> getTracks();
	Collection<Track> getExistingTracksForUser(User user);
	int getLastTrackId();
	Track getTrackById(int id);
	boolean persist(Track track);
	boolean update(Track track);
	boolean delete(Integer trackid);
	void persist(Collection<Track> tracks);
	Collection<Object[]> getMusic(User user);
	Collection<Object[]> getVideo(User thisUser);
}

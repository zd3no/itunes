package dao;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import model.Playlist;
import model.PlaylistTrack;
import model.Track;
import model.User;

@Local
@Stateless
public class PlaylistImplDAO implements PlaylistDAO {
	
	@PersistenceContext(type=PersistenceContextType.EXTENDED)
	private EntityManager em;

	@Override
	public Collection<Playlist> getPlaylists() {
		return em.createQuery("Select p From Playlist p", Playlist.class).getResultList();
	}

	@Override
	public Playlist getPlaylistById(String id) {
		Collection<Playlist> playlists = em.createQuery("Select p from Playlist p Where p.persistentId = :playlistId",Playlist.class).setParameter("playlistId", id).getResultList();
		return playlists.size() > 0 ? playlists.iterator().next() : null;
	}

	@Override
	public boolean persist(Playlist playlist) {
		if (playlist == null)
			return false;
		try {
			em.persist(playlist);
			return true;
		}catch(EntityExistsException e) {
			return false;
		}
	}
	
	@Override
	public void update(Collection<Playlist> playlists) {
		if (playlists == null)
			return;
		try {
			for (Playlist playlist : playlists) {
				delete(playlist);
				em.persist(playlist);
			}
		}catch(EntityExistsException e) {
			//do nothing
		}
	}

	@Override
	public boolean update(Playlist playlist) {
		if (playlist == null)
			return false;
		Playlist newPlaylist = em.merge(playlist);
		if (newPlaylist.equals(playlist))
			return true;
		else
			return false;
	}

	@Override
	public boolean delete(Playlist playlist) {
		Playlist toRemove = em.find(Playlist.class,playlist.getPersistentId());
		if (toRemove != null) {
			em.remove(toRemove);
			return true;
		}else
			return false;
	}

	@Override
	public Map<String, Playlist> getPlaylistsForUser(User user) {
		Collection<Playlist> playlists = em.createQuery("SELECT p FROM Playlist p WHERE p.user=:user",Playlist.class).setParameter("user", user).getResultList();
		Map<String, Playlist> playlistMap = new HashMap<>();
		for (Playlist playlist : playlists) {
			playlistMap.put(playlist.getPersistentId(), playlist);
		}
		return playlistMap;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Collection<String[]> getPlaylistNamesForUser(User user) {
		return em.createQuery("SELECT p.persistentId, p.name FROM Playlist p WHERE p.user=:user").setParameter("user", user).getResultList();
	}

	@Override
	public Collection<String> getPlaylistsContainingTrack(Integer trackId, User user) {
		return em.createQuery("SELECT DISTINCT p.name FROM Playlist as p inner join p.tracks as pt "
				+ "WHERE p.user = :user AND pt.playlist.persistentId = p.persistentId AND "
				+ "pt.track.trackId = :trackId",String.class).setParameter("trackId", trackId).setParameter("user", user).getResultList();
	}

	@Override
	public Collection<Track> getMyTracks(User thisUser, String persistentId) {
		Playlist mPlaylist = em.find(Playlist.class, persistentId);
		if(mPlaylist==null)
			return null;
		return em.createQuery("Select t from PlaylistTrack pt inner join pt.playlist as p inner join pt.track as t "
				+ "where p.persistentId =:persistentId "
				+ "and p.persistentId = pt.playlist.persistentId and t.trackId = pt.track.trackId",Track.class).setParameter("persistentId", persistentId).getResultList();
	}

	@Override
	public boolean remove(Track track) {
		Track mTrack = em.find(Track.class, track.getTrackId());
		if (mTrack == null) {
			return false;
		}
		Collection<PlaylistTrack> list = em.createQuery("Select pt from PlaylistTrack pt where pt.track =:track",PlaylistTrack.class).setParameter("track", mTrack).getResultList();
		for (PlaylistTrack playlistTrack : list) {
			em.remove(playlistTrack);
		}
		return true;
	}

	@Override
	public boolean removeTrackFromPlaylistOnly(Integer trackId,
			String playlistID) {
		Track mTrack = em.find(Track.class	, trackId);
		Playlist playlist = em.find(Playlist.class, playlistID);
		if(mTrack == null || playlist == null)
			return false;
		Collection<PlaylistTrack> list = em.createQuery("Select pt from PlaylistTrack pt"
				+ " where pt.track =:track AND pt.playlist=:playlist",
				PlaylistTrack.class).setParameter("track", mTrack)
				.setParameter("playlist", playlist)
				.getResultList();
		if (list.size()==0) 
			return false;
		playlist.removeTrack(list.iterator().next());
		em.merge(playlist);
		em.remove(list.iterator().next());
		
		return true;
	}

	@Override
	public boolean addTrackToPlaylist(Integer trackId, String playlistId) {
		Playlist playlist = em.find(Playlist.class, playlistId);
		Track track = em.find(Track.class, trackId);
		if(playlist == null || track==null)
			return false;
		playlist.addTrack(track);
		em.merge(playlist);
		return true;
	}

}

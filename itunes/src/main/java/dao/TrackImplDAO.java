package dao;

import java.util.Collection;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.PlaylistTrack;
import model.Track;
import model.User;

@Stateless
@Local
public class TrackImplDAO implements TrackDAO {
	
	@PersistenceContext
	private EntityManager em;

	public TrackImplDAO() {
	}

	@Override
	public Collection<Track> getTracks() {
		return em.createQuery("Select t From Track t",Track.class).getResultList();
	}

	@Override
	public Track getTrackById(int id) {
		Collection<Track> tracks = em.createQuery("Select t from Track t Where t.trackId = :trackId",Track.class).setParameter("trackId", id).getResultList();
		return tracks.size() > 0 ? tracks.iterator().next() : null;
	}

	@Override
	public boolean persist(Track track) {
		if (track == null)
			return false;
		try {
			em.persist(track);
			return true;
		}catch(EntityExistsException e) {
			return false;
		}
	}
	
	@Override
	public void persist(Collection<Track> tracks) {
		if (tracks == null)
			return;
		try {
			for (Track track : tracks) {
				em.persist(track);
			}
		}catch(EntityExistsException e) {
			//do nothing
		}
	}

	@Override
	public boolean update(Track track) {
		if (track == null)
			return false;
		Track newTrack = em.merge(track);
		if (newTrack.equals(track))
			return true;
		else
			return false;
	}

	@Override
	public boolean delete(Integer trackId) {
		if (trackId == null)
			return false;
		Track toRemove = em.find(Track.class, trackId);
		if (toRemove != null) {
			Collection<PlaylistTrack> list = em.createQuery("Select pt from PlaylistTrack pt inner join pt.track as t where t.trackId =:trackId",
					PlaylistTrack.class).setParameter("trackId", trackId).getResultList();
			for (PlaylistTrack playlistTrack : list) {
				playlistTrack.getPlaylist().removeTrack(playlistTrack);
				em.merge(playlistTrack.getPlaylist());
				em.remove(playlistTrack);
			}
			em.remove(toRemove);
			return true;
		}else
			return false;
	}

	@Override
	public Collection<Track> getExistingTracksForUser(User user) {
		return em.createQuery("SELECT t FROM Track t WHERE t.user =:user",Track.class).setParameter("user", user).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Collection<Object[]> getMusic(User user){
		return em.createQuery("SELECT t.trackId, t.name, t.artist, t.album, t.genre, t.kind, t.composer, t.size, t.totalTime, t.dateAdded, t.dateModified, t.bitRate,"
				+ "t.sampleRate FROM Track t WHERE t.user =:user AND t.videoHeight=0").setParameter("user", user).getResultList();
	}

	@Override
	public int getLastTrackId() {
		List<Integer> lastId =  em.createQuery("SELECT t.trackId FROM Track t ORDER BY t.trackId DESC",Integer.class).setMaxResults(1).getResultList();
		int last;
		if (lastId.size() != 0) {
			last = lastId.get(0);
		} else {
			last = 0;
		}
		return last;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Collection<Object[]> getVideo(User thisUser) {
		return em.createQuery("SELECT t.trackId, t.name, t.artist, t.album, t.genre, t.kind, t.size, t.totalTime, t.dateAdded, t.dateModified, t.hd,"
				+ "t.videoWidth, t.videoHeight FROM Track t WHERE t.user =:user AND t.videoHeight>0").setParameter("user", thisUser).getResultList();
	}

}

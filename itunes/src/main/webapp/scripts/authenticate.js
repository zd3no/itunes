function getXmlHttp(){
	var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	return xmlhttp;
}

function switchToRegister(){
	var form = document.getElementById("authenticate_form");
	form.innerHTML = 
		"<input type='text' id='name_of_user' required placeholder='Name'><br>"
		+"<input type='text' id='username' required placeholder='Username/ID'><br>"
		+"<input type='password' id='password' required placeholder='Password'><br>"
		+"<input type='password' id='password_confirm' required placeholder='Confirm Password'><br>"
		+"<input type='submit' id='btn_register' value='Register' onclick='register()'>"
		+"<p id='error'></p>"
		+"<input type='button' class='switchAuthForm' id='switch_to_login' value='Login' onclick='switchToLogin()'>"
	form.style.display = "block";
}

function switchToLogin(){
	var form = document.getElementById("authenticate_form");
	form.innerHTML = 
		"<input type='text' id='username' required placeholder='Username/ID'><br>"
		+"<input type='password' id='password' required placeholder='Password'><br>"
		+"<input type='submit' id='btn_login' value='Login' onclick='login()'>"
		+"<p id='error'></p>"
		+"<input type='button' class='switchAuthForm' id='switch_to_register' value='Register' onclick='switchToRegister()'>";
	form.style.display = "block";
}

function register(){
	var name = document.getElementById("name_of_user");
	var username = document.getElementById("username");
	var password = document.getElementById("password");
	var passwordConfirm = document.getElementById("password_confirm");
	var notifier = document.getElementById("error");
	
	//reset validation errors
	name.setCustomValidity("");
	username.setCustomValidity("");
	password.setCustomValidity("");
	passwordConfirm.setCustomValidity("");
	document.getElementById("error").style.visibility = "hidden";
	//validation
	if(name.value=="" || username.value=="" || password.value=="" || passwordConfirm.value==""){
		return; //let html5 required handle it
	}
	if(!name.value.match("^[a-zA-Z\\s\\']+$")){
		name.setCustomValidity("Invalid Name!");
		return;
	}else if(username.value.length < 6){
		username.setCustomValidity("User ID is too short! Min 6 chars");
		return;
	}else if(password.value.length < 6){
		password.setCustomValidity("Password is too short! Min 6 chars");
		return;
	}else if(password.value != passwordConfirm.value){
		passwordConfirm.setCustomValidity("Passwords do not match!");
		return;
	}
	
	var xmlhttp = getXmlHttp();
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4){
			if(xmlhttp.status == 200) {
				var jsonUser = JSON.parse(xmlhttp.responseText);
				setCookie("user",JSON.stringify(jsonUser),1);
				loadUserPage();
			}else{
				document.getElementById("error").style.visibility = "visible";
				document.getElementById("error").innerHTML = "*"+xmlhttp.responseText;
				return;
			}
		}
	};
	xmlhttp.open("POST", "rest/authenticate/register", true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send("userId="+username.value+"&name="+name.value+"&password="+password.value);
}

function login(){
	var username = document.getElementById("username");
	var password = document.getElementById("password");
	var loginButton = document.getElementById("btn_login");
	var strUsername = username.value;
	var strPassword = password.value;
	
	//validation
	username.setCustomValidity("");
	password.setCustomValidity("");
	document.getElementById("error").style.visibility = "hidden";
	if (strUsername.length ==0) {
		username.setCustomValidity("Username/ID must not be null");
		return;
	}else if (strPassword.length == 0){
		password.setCustomValidity("Password cannot be null");
		return;
	}
	
	var xmlhttp = getXmlHttp();
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4){
			if(xmlhttp.status == 200) {
				var jsonUser = JSON.parse(xmlhttp.responseText);
				setCookie("user",JSON.stringify(jsonUser),1);
				loadUserPage();
			}else{
				document.getElementById("error").style.visibility = "visible";
				document.getElementById("error").innerHTML = "*Invalid login "+xmlhttp.responseText;
				return;
			}
		}
	};
	xmlhttp.open("GET", "rest/authenticate/login?username="+strUsername+"&password="+strPassword, true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send();
}

/**
 * Store the cookie.
 * @param cname - cookie name
 * @param cvalue - cookie value
 * @param exdays - amount of days set for cookie to expire
 */
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

/**
 * Returns the cookie value by name
 * @param cname
 * @returns
 */
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}
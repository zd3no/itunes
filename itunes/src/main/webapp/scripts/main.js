var display;
var selectedRowId;
var selectedRow;
var action;
var myTable;
var selectedRowInternalIndex;

window.onload=function(){
	display = document.getElementById("display");
	var cookie = getCookie("user");
	if(cookie != ""){
		loadUserPage();
		getAllPlaylists();
	}else{
		switchToLogin();
		document.getElementsByTagName("nav")[0].style.visibility="hidden";
		document.getElementById("main_nav").style.display = "none";
	}
};

function loadUserPage(){
	document.getElementsByTagName("nav")[0].style.visibility="visible";
	document.getElementById("authenticate_form").style.display="none";
	document.getElementById("user_name").innerHTML=JSON.parse(getCookie("user")).name;
	document.getElementById("main_nav").style.display="block";
}

function editProfile(){
	document.getElementById("profileForm").style.display="block";
	var user = JSON.parse(getCookie("user"));
	document.getElementById("u_name").value = user.name;
	document.getElementById("u_id").value = user.userId;
	document.getElementById("u_password").value = user.password;
	document.getElementById("u_password_confirm").value = user.password;
	document.getElementById("u_library_id").value = user.liraryId;
}

function submitUserDetails(){
	//validation
	var userId = document.getElementById("u_id");
	var username = document.getElementById("u_name");
	var userPassword = document.getElementById("u_password");
	var userPasswordConfirm = document.getElementById("u_password_confirm");
	userPasswordConfirm.setCustomValidity("");
	if(username.value == "" || userPassword.value == "" || userId=="")
		return;
	if(userPassword.value != userPasswordConfirm.value){
		userPasswordConfirm.setCustomValidity("Passwords do not match!");
		return;
	}
	$.ajax({
	    type: "POST", 
	    url: "rest/query/updateUser",
	    dataType: 'text',
	    data: { name: username.value, oldUserId:JSON.parse(getCookie("user")).userId, newUserId:userId.value, password: userPassword.value },
	}).done(function(data) {
		document.getElementById("profileForm").style.display="none";
		setCookie("user",JSON.stringify(JSON.parse(data)),30);
		location.reload();
	}).fail(function() {
		document.getElementById("profileForm").style.display="none";
	    alert("Sorry. Server unavailable. ");
	});
}

function logout(){
	setCookie("user",null,-1);
	location.reload(true);
}

function showUpload(){
	clearDisplay();
	display.appendChild(upload_form);
}

function clearDisplay(){
	divs = display.getElementsByTagName("DIV");
	for (var int = 0; int < divs.length; int++) {
		display.removeChild(divs[int]);
	}
	
}

function showUploadForm(){
	clearDisplay();
	display.appendChild(upload_form);
}

function uploadFile() {
	var fileSelect = document.getElementById('selectedFile');
	var uploadButton = document.getElementById('btn_uploadFile');
	var error = document.getElementById("upload_file_status");
	error.style.display = "none";
	var file = fileSelect.files[0];
	try {
		console.log(file.type);
		var formData = new FormData();
		if (!file.type.match('text/xml')) {
			error.style.display = "inline-block";
			error.innerHTML = "Invalid File Selected!";
			return;
		}
	} catch (error) {
		return;
	}
	formData.append('uploadedFile', file, file.name);
	var xhr = new XMLHttpRequest();
	xhr.open('POST', 'rest/parse/uploadFile', true);
	xhr.setRequestHeader("user",JSON.parse(getCookie("user")).userId);
	xhr.send(formData);
	uploadButton.value = "Uploading...";
	xhr.onload = function() {
		if (xhr.status === 200) {
			uploadButton.value = "Upload";
			uploadButton.disabled = false;
			error.style.display = "inline-block";
			error.style.color="Green";
			error.innerHTML = "Uploaded";
			fileSelect.value = null;
			setCookie("user",JSON.stringify(JSON.parse(xhr.responseText)), 30);
		} else {
			error.style.display = "inline-block";
			error.style.color="Red";
			error.innerHTML = xhr.responseText;
		}
	};
}

function myLibrary(){
	var canvas = document.getElementById("canvas");
	var leftNav = document.getElementById("left_nav");
	var content = document.getElementById("content");
	
	canvas.style.display = "block";
}

function getAllTracks(e){
	setCookie("playlistID",e.id,30);
	var xhr = new XMLHttpRequest();
	xhr.open('GET', 'rest/query/getAllAudioFiles', true);
	xhr.setRequestHeader("user",JSON.parse(getCookie("user")).userId);
	xhr.send();
	xhr.onload = function() {
		if (xhr.status === 200) {
			var response = JSON.parse(xhr.responseText);
			$('#content').html( '<table cellspacing="0" id="queryResults"></table>' );
			myTable = $('#queryResults').dataTable( {
		        data: response,
		        bAutoWidth: false,
		        bPaginate : true,
		        deferRender: true,
		        search: true,
		        sort:true,
		        fnDrawCallback: rebindDTevents,
		        columns: [
		            { title : "ID", visible:false},
		            { title : "NAME"},
		            { title : "ARTIST"},
		            { title : "ALBUM"},
		            { title : "GENRE"},
		            { title : "KIND"},
		            { title : "COMPOSER"},
		            { title : "SIZE", "render": ParseSize},
		            { title : "DURATION", "render": ParseDuration},
		            { title : "ADDED", "render": ParseDateColumn},
		            { title : "MODIFIED", "render": ParseDateColumn},
		            { title : "BIT RATE", "render": Kbps},
		            { title : "SAMPLE RATE", "render": Hz}],
				aoColumnDefs: [{ "sDefaultContent": '-', "aTargets": [ '_all' ]}]
			} );    
			action = "editingGlobalTrack";
		} else {
			alert("error");
		}
	};
}

function getAllVideo(e){
	setCookie("playlistID",e.id,30);
	var xhr = new XMLHttpRequest();
	xhr.open('GET', 'rest/query/getAllVideoFiles', true);
	xhr.setRequestHeader("user",JSON.parse(getCookie("user")).userId);
	xhr.send();
	xhr.onload = function() {
		if (xhr.status === 200) {
			var response = JSON.parse(xhr.responseText);
			$('#content').html( '<table cellspacing="0" id="queryResults"></table>' );
			myTable = $('#queryResults').dataTable( {
		        data: response,
		        bAutoWidth: false,
		        bPaginate : true,
		        deferRender: true,
		        search: true,
		        sort:true,
		        fnDrawCallback: rebindDTevents,
		        columns: [
		            { title : "ID", visible:false},
		            { title : "NAME"},
		            { title : "ARTIST"},
		            { title : "ALBUM"},
		            { title : "GENRE"},
		            { title : "KIND"},
		            { title : "SIZE", "render": ParseSize},
		            { title : "DURATION", "render": ParseDuration},
		            { title : "ADDED", "render": ParseDateColumn},
		            { title : "MODIFIED", "render": ParseDateColumn},
		            { title : "HD"},
		            { title : "WIDTH"},
		            { title : "HEIGHT"}],
				aoColumnDefs: [{ "sDefaultContent": '-', "aTargets": [ '_all' ]}]
			} );    
			action = "editingGlobalTrack";
		} else {
			alert("error");
		}
	};
}

rebindDTevents = function(){
	$("#queryResults tbody tr td").on("dblclick", function(event){
		clearHighlightsOnTables();
		selectedRowId = myTable.fnGetData(myTable.fnGetPosition(this)[0])[0];
		if(typeof selectedRowId === 'undefined')
			selectedRowId = myTable.fnGetData(myTable.fnGetPosition(this)[0]).trackId;
		selectedRowInternalIndex = $('#queryResults').DataTable().row( this.parentNode ).index();
		var thisTd = this;
		selectedRow = this.parentNode;
		var offset = $(this).offset();
		var left = event.pageX;
		var top = event.pageY;
		var theHeight = $('#poppie').height();
		if (action == "editingLocalTrack") {
			var popDeleteButton = document.getElementById('popDeleteButton');
			popDeleteButton.removeEventListener("click",showGlobalTrackDeleteConfirmation);
			popDeleteButton.value = "Remove";
			popDeleteButton.addEventListener("click",showLocalTrackConfirmation);
		}else{
			var popDeleteButton = document.getElementById('popDeleteButton');
			popDeleteButton.removeEventListener("click",showLocalTrackConfirmation);
			popDeleteButton.value = "Delete";
			popDeleteButton.addEventListener("click",showGlobalTrackDeleteConfirmation);
		}
		$.get( "rest/query/getPlaylistsOfTrack", { id: selectedRowId, user: JSON.parse(getCookie("user")).userId } )
		.done(function( data ) {
			thisTd.parentNode.style.color = '#B67500';
			var popup = $('#poppie');
			popup.show();
			popup.css('left', (left+10) + 'px');
			popup.css('top', (top-(theHeight/2)-10) + 'px');
			contentDiv = document.getElementById("popContent");
			string = "<ul>";
			for(var i=0; i<data.length; i++){
				string+="<li>"+data[i]+"</li>";
			}
			string+="</ul>";
			contentDiv.innerHTML = string;
			if(document.getElementById(getCookie("playlistID")).innerHTML == "All Music" || document.getElementById(getCookie("playlistID")).innerHTML == "All Video"){
				document.getElementById("popMoveButton").style.display="none";
			}else{
				document.getElementById("popMoveButton").style.display="block";
			}
		});
	    });
	$("#queryResults tbody tr td").on("click", function(event){
		clearHighlightsOnTables();
			this.parentNode.style.color = '#B67500';
	    });
}

function showEditor(){
	document.getElementById("editor").style.display = "block";
	document.getElementById("editor_buttons").style.display = "block";
	document.getElementById("poppie").style.display='none';
	document.getElementById("tint").style.display = "block";
	if(action == "editingGlobalTrack"){
		generateGlobalTrackEditorContent();
	}else if(action == "editingLocalTrack"){
		generateGlobalTrackEditorContent();
	}else if(action == "addingPlaylist"){
		generateAddingPlaylist();
	}
}

function editRow(){
	if(action == "editingGlobalTrack" || action == "editingLocalTrack"){
		//validation
		if(document.getElementById("track_name").value==""){
			document.getElementById("track_name").setCustomValidity("Please fill Track Name");
			return;
		}else if(document.getElementById("track_artist").value==""){
			document.getElementById("track_artist").setCustomValidity("Please fill Artist");
			return;
		}else if(document.getElementById("track_album").value==""){
			document.getElementById("track_album").setCustomValidity("Please fill Album");
			return;
		}else if(document.getElementById("track_genre").value==""){
			document.getElementById("track_genre").setCustomValidity("Please fill Genre");
			return;
		}else if(document.getElementById("track_composer").value==""){
			document.getElementById("track_composer").setCustomValidity("Please fill Composer");
			return;
		}
		xtrackName = document.getElementById("track_name").value;
		xtrackArtist = document.getElementById("track_artist").value;
		xtrackAlbum = document.getElementById("track_album").value;
		xtrackGenre = document.getElementById("track_genre").value;
		xtrackComposer = document.getElementById("track_composer").value;
		editGlobalTrack();
	}else if(action == "addingPlaylist"){
		if(document.getElementById("playlistName").value==""){
			document.getElementById("playlistName").setCustomValidity("Please fill Playlist Name");
			return;
		}
		createPlaylist();
	}else if(action=="editingPlaylist"){
		if(document.getElementById("playlistName").value==""){
			document.getElementById("playlistName").setCustomValidity("Please fill Playlist Name");
			return;
		}
		performEditActionOnPlaylist();
	}
}

function editGlobalTrack(){
	$.ajax({
	    type: "POST", 
	    url: "rest/query/editTrack",
	    dataType: 'text',
	    data: { id: selectedRowId, name: xtrackName, artist: xtrackArtist, album: xtrackAlbum, genre: xtrackGenre, composer: xtrackComposer },
	}).done(function() {
		closeEditor();
		clearHighlightsOnTables();
		myTable.fnUpdate( xtrackName, selectedRowInternalIndex, 1);
		myTable.fnUpdate( xtrackArtist, selectedRowInternalIndex, 2);
		myTable.fnUpdate( xtrackAlbum, selectedRowInternalIndex, 3);
		myTable.fnUpdate( xtrackGenre, selectedRowInternalIndex, 4);
		myTable.fnUpdate( xtrackComposer, selectedRowInternalIndex, 6);
		myTable.fnUpdate( new Date().toLocaleDateString()+" "+ new Date().toLocaleTimeString(), selectedRowInternalIndex, 10);
	}).fail(function() {
	    alert("Sorry. Server unavailable. ");
	});
}

function performEditActionOnPlaylist(){
	document.getElementById("triangles").style.display="block";
	var xhr = new XMLHttpRequest();
	xhr.open('GET', 'rest/query/editPlaylistName?playlistName='+document.getElementById("playlistName").value
			+'&playlistId='+getCookie("playlistID"), true);
	xhr.setRequestHeader("user",JSON.parse(getCookie("user")).userId);
	xhr.send();
	xhr.onload = function() {
		if (xhr.status === 200) {
			var response = JSON.parse(xhr.responseText);
			closeEditor();
			var li = document.getElementById(response.persistentId);
			li.innerHTML = response.name;
			document.getElementById("triangles").style.display="none";
		} else {
			alert("Sorry. Server unavailable. ");
			document.getElementById("triangles").style.display="none";
		}
	};
}

function createPlaylist(){
	var xhr = new XMLHttpRequest();
	xhr.open('GET', 'rest/query/createPlaylist?playlistName='+document.getElementById("playlistName").value, true);
	xhr.setRequestHeader("user",JSON.parse(getCookie("user")).userId);
	xhr.send();
	xhr.onload = function() {
		if (xhr.status === 200) {
			closeEditor();
			var playlistUL=document.getElementById("playlistList");
			var li = document.createElement("LI");
			var response = JSON.parse(xhr.responseText);
			li.innerHTML = response.name;
			li.id=response.persistentId;
			li.addEventListener("click",getTracksForThisPlaylist(this),true);
			playlistUL.appendChild(li);
		} else {
			alert("Sorry. Server unavailable. ");
		}
	};
}

var xtrackName, xtrackArtist, xtrackAlbum, xtrackGenre, xtrackComposer;

function generateGlobalTrackEditorContent(){
	document.getElementById("editor_content").innerHTML = trackEditContent;
	document.getElementById("editor_title").innerHTML = "Edit Track:";
	xtrackName = document.getElementById("track_name").value = $("td:eq(0)", selectedRow).text();
	xtrackArtist = document.getElementById("track_artist").value = $("td:eq(1)", selectedRow).text();
	xtrackAlbum = document.getElementById("track_album").value = $("td:eq(2)", selectedRow).text();
	xtrackGenre = document.getElementById("track_genre").value = $("td:eq(3)", selectedRow).text();
	xtrackComposer = document.getElementById("track_composer").value = $("td:eq(5)", selectedRow).text();
}

function closeEditor(){
	document.getElementById("editor").style.display = "none";
	document.getElementById("tint").style.display = "none";
}

function cancelEditor(){
	document.getElementById("editor").style.display = "none";
	document.getElementById("tint").style.display = "none";
}

function clearHighlightsOnTables(){
	document.getElementById("poppie").style.display='none';
	var list = document.getElementsByTagName("TR");
	for (var i = 0; i < list.length; i++) {
		list[i].style.color="#666";
	}
}

function ParseDateColumn(data, type, row) {
	date = new Date(data);
	return date.toLocaleDateString()+" "+ date.toLocaleTimeString();
}

function ParseDuration(data, type, row) {
	value = parseInt(data);
	hour = Math.floor(value / (1000*60*60));
	minute = Math.floor((value - (hour*1000*60*60)) / (1000*60));
	second = Math.floor((value - (hour*1000*60*60) - (minute*1000*60)) / 1000);
	shour = hour>0?(hour+":"):"";
	sminute = minute<10?("0"+minute):minute;
	sminute = sminute=="0"?"00:":sminute+":";
	ssecond = second<10?("0"+second):second;
	return shour+sminute+ssecond;
}

function ParseSize(data, type, row) {
	value = parseFloat(data);
	MiB = value / Math.pow(2,20);
	return MiB.toFixed(1)+" MB"
}

function Hz(data, type, row) {
	value = parseInt(data);
	return value+" Hz"
}

function Kbps(data, type, row) {
	value = parseInt(data);
	return value+" kbps"
}


function getAllPlaylists(){
	var xhr = new XMLHttpRequest();
	xhr.open('GET', 'rest/query/getAllPlaylists', true);
	xhr.setRequestHeader("user",JSON.parse(getCookie("user")).userId);
	xhr.send();
	xhr.onload = function() {
		if (xhr.status === 200) {
			var response = JSON.parse(xhr.responseText);
			var playlistsdiv = document.getElementById("playlists");
			var string = "<ul id='playlistList'>";
			for (var i = 0; i < response.length; i++) {
				console.log(response[i][0],response[i][1]);
				string+='<li id="'+response[i][0]+'" onclick="getTracksForThisPlaylist(this)">'+response[i][1]+'</li>';
			}
			string+="</ul>";
			playlistsdiv.innerHTML= string;
		} else {
			alert("Server error!");
		}
	};
}

function getTracksForThisPlaylist(e){
	setCookie("playlistID",e.id,30);
	if(action == "editingPlaylist"){
		generateEditingPlaylist(e.id);
		showEditor();
		return;
	}
	if(action=="deletingPlaylist"){
		showPlaylistDeleteConfirmation();
		return;
	}
	var xhr = new XMLHttpRequest();
	xhr.open('GET', 'rest/query/getTracksForPlaylist?persistentId='+e.id, true);
	xhr.setRequestHeader("user",JSON.parse(getCookie("user")).userId);
	xhr.send();
	xhr.onload = function() {
		if (xhr.status === 200) {
			var response = JSON.parse(xhr.responseText);
			$('#content').html( '<table cellspacing="0" id="queryResults"></table>' );
			myTable = $('#queryResults').dataTable( {
		        data: response,
		        bAutoWidth: false,
		        bPaginate : true,
		        deferRender: true,
		        search: true,
		        sort:true,
		        fnDrawCallback: rebindDTevents,
		        columns: [
		            { data : "id", title : "ID", visible:false},
		            { data : "name", title : "NAME"},
		            { data : "artist", title : "ARTIST"},
		            { data : "album", title : "ALBUM"},
		            { data : "genre", title : "GENRE"},
		            { data : "kind", title : "KIND"},
		            { data : "composer", title : "COMPOSER"},
		            { data : "size", title : "SIZE", "render": ParseSize},
		            { data : "totalTime", title : "DURATION", "render": ParseDuration},
		            { data : "dateAdded", title : "ADDED", "render": ParseDateColumn},
		            { data : "dateModified", title : "MODIFIED", "render": ParseDateColumn},
		            { data : "bitRate", title : "BIT RATE", "render": Kbps},
		            { data : "sampleRate", title : "SAMPLE RATE", "render": Hz},
		            { data : "playCount", title : "#PLAYED"},
		            { data : "hasVideo", title : "VIDEO"},
		            { data : "hd", title : "HD"}],
				aoColumnDefs: [{ "sDefaultContent": '-', "aTargets": [ '_all' ]}]
			} );    
			action = "editingLocalTrack";
		} else {
			alert("Server Error!");
		}
	};
}

function showPlaylistDeleteConfirmation(){
	document.getElementById("confirmPop").style.display = "block";
	document.getElementById("tint").style.display = "block";
	document.getElementById("confirmContent").innerHTML = "Are you sure you want to delete this playlist?";
}

function showGlobalTrackDeleteConfirmation(){
	document.getElementById("confirmPop").style.display = "block";
	document.getElementById("tint").style.display = "block";
	document.getElementById("confirmContent").innerHTML = "Delete track from all playlists?";
}

function showLocalTrackConfirmation(){
	document.getElementById("confirmPop").style.display = "block";
	document.getElementById("tint").style.display = "block";
	document.getElementById("confirmContent").innerHTML = "Delete track from "+document.getElementById(getCookie("playlistID")).innerHTML+"?";
}

function hideConfirmPopup(){
	document.getElementById("confirmPop").style.display = "none";
	document.getElementById("tint").style.display = "none";
}

function proceedConfirmPopup(){
	if(action=="deletingPlaylist"){
		document.getElementById("triangles").style.display="block";
		$.ajax({
		    type: "POST", 
		    url: "rest/query/deletePlaylist",
		    dataType: 'text',
		    data: { playlistId: getCookie("playlistID") },
		}).done(function() {
			document.getElementById("triangles").style.display="none";
			hideConfirmPopup();
			clearHighlightsOnTables();
			var toRemove = document.getElementById(getCookie("playlistID"));
			toRemove.parentNode.removeChild(toRemove);
		}).fail(function() {
			document.getElementById("triangles").style.display="none";
			hideConfirmPopup()
		    alert("Sorry. Server unavailable. ");
		});
		return;
	}
	if(action=="editingLocalTrack"){
		$.ajax({
		    type: "POST", 
		    url: "rest/query/removeTrackFromPlaylistOnly",
		    dataType: 'text',
		    data: { id: selectedRowId, playlistId: getCookie("playlistID") },
		}).done(function() {
			hideConfirmPopup();
			clearHighlightsOnTables();
			$('#queryResults').DataTable().row(selectedRow).remove().draw();
		}).fail(function() {
			hideConfirmPopup()
		    alert("Sorry. Server unavailable. ");
		});
		return;
	}
	if(action == "editingGlobalTrack"){
		document.getElementById("triangles").style.display="block";
		$.ajax({
			type: "POST", 
			url: "rest/query/deleteTrack",
			dataType: 'text',
			data: { id: selectedRowId },
		}).done(function() {
			hideConfirmPopup();
			clearHighlightsOnTables();
			document.getElementById("triangles").style.display="none";
			$('#queryResults').DataTable().row(selectedRow).remove().draw();
		}).fail(function() {
			hideConfirmPopup()
			document.getElementById("triangles").style.display="none";
			alert("Sorry. Server unavailable. ");
		});
		return;
	}
}

function addPlaylist(){
	action = "addingPlaylist";
	showEditor();
}

function generateAddingPlaylist(){
	document.getElementById("editor_content").innerHTML = playlistCreation;
	document.getElementById("editor_title").innerHTML = "New Playlist";
	document.getElementById("editor_editButton").value = "Add";
}

function generateEditingPlaylist(id){
	document.getElementById("editor_content").innerHTML = playlistCreation;
	document.getElementById("editor_title").innerHTML = "Selected Playlist";
	document.getElementById("playlistName").value = document.getElementById(getCookie("playlistID")).innerHTML;
	document.getElementById("editor_editButton").value = "Edit";
}

function editPlaylist(){
	document.getElementById("editModeActive").style.display="block";
	action="editingPlaylist";
}

function exitEditMode(){
	document.getElementById("editModeActive").style.display="none";
	action="";
}

function removePlaylist(){
	document.getElementById("deleteModeActive").style.display="block";
	action="deletingPlaylist";
}

function exitDeleteMode(){
	document.getElementById("deleteModeActive").style.display="none";
	action="";
}

function addTo(){
	document.getElementById("editor").style.display="block";
	document.getElementById("editor_title").innerHTML = "Pick a playlist:";
	document.getElementById("editor_buttons").style.display = "none";
	var xhr = new XMLHttpRequest();
	xhr.open('GET', 'rest/query/getAllPlaylists', true);
	xhr.setRequestHeader("user",JSON.parse(getCookie("user")).userId);
	xhr.send();
	xhr.onload = function() {
		if (xhr.status === 200) {
			var response = JSON.parse(xhr.responseText);
			var content = "<ul>";
			for (var i = 0; i < response.length; i++) {
				content+='<li id="'+response[i][0]+'" onclick="addTrackToThisPlaylist(this)">'+response[i][1]+'</li>';
			}
			content+="</ul>";
			document.getElementById("editor_content").innerHTML = content;
		} else {
			alert("Server error!");
			closeEditor();
		}
	};
}

function addTrackToThisPlaylist(e){
	var persistentId = e.id;
	var trackId = selectedRowId;
	$.ajax({
	    type: "POST", 
	    url: "rest/query/addTrackToThisPlaylist",
	    dataType: 'text',
	    data: { trackId: trackId, playlistId: persistentId },
	}).done(function() {
		closeEditor();
		document.getElementById("poppie").style.display='none';
	}).fail(function() {
		closeEditor();
		document.getElementById("poppie").style.display='none';
	    alert("Sorry. Server unavailable. ");
	});
}

function moveTrackToThisPlaylist(e){
	var persistentId = e.id;
	var trackId = selectedRowId;
	$.ajax({
	    type: "POST", 
	    url: "rest/query/addTrackToThisPlaylist",
	    dataType: 'text',
	    data: { trackId: trackId, playlistId: persistentId },
	}).done(function() {
		$.ajax({
		    type: "POST", 
		    url: "rest/query/removeTrackFromPlaylistOnly",
		    dataType: 'text',
		    data: { id: trackId, playlistId: getCookie("playlistID") },
		}).done(function() {
			hideConfirmPopup();
			clearHighlightsOnTables();
			$('#queryResults').DataTable().row(selectedRow).remove().draw();
		}).fail(function() {
			hideConfirmPopup()
		    alert("Sorry. Server unavailable. ");
		});
		closeEditor();
		document.getElementById("poppie").style.display='none';
	}).fail(function() {
		closeEditor();
		document.getElementById("poppie").style.display='none';
	    alert("Sorry. Server unavailable. ");
	});
}

function moveTo(){
	document.getElementById("editor").style.display="block";
	document.getElementById("editor_title").innerHTML = "Pick a playlist:";
	document.getElementById("editor_buttons").style.display = "none";
	var xhr = new XMLHttpRequest();
	xhr.open('GET', 'rest/query/getAllPlaylists', true);
	xhr.setRequestHeader("user",JSON.parse(getCookie("user")).userId);
	xhr.send();
	xhr.onload = function() {
		if (xhr.status === 200) {
			var response = JSON.parse(xhr.responseText);
			var content = "<ul>";
			for (var i = 0; i < response.length; i++) {
				if(response[i][0]==getCookie("playlistID"))
					continue;
				content+='<li id="'+response[i][0]+'" onclick="moveTrackToThisPlaylist(this)">'+response[i][1]+'</li>';
			}
			content+="</ul>";
			document.getElementById("editor_content").innerHTML = content;
		} else {
			alert("Server error!");
			closeEditor();
		}
	};
}

upload_form = document.createElement("div");
upload_form.id="upload_form";
upload_form.setAttribute("class","upload_form");
upload_form.innerHTML = 
	"<h2>Upload xml playlist</h2>"
		+"<div id='uploadFile_form'>"
		+"<form action='rest/parse/uploadFile' method='post'"
		+"enctype='multipart/form-data;charset=\"UTF-8\"' id='upload_file'"
		+"onsubmit='uploadFile(); return false;' acceptcharset='UTF-8'>"
		+"<input id='selectedFile' type='file' name='uploadedFile' size='50' /><br>"
		+"<input type='submit' class='button' value='Upload' id='btn_uploadFile' /> <br>"
		+"<p style='display: none' id='upload_file_status'></p>"
		+"</form>"
		+"</div>";

var trackEditContent = 
	'<label>Name:</label><input id="track_name" type="text" required><br>'+
	'<label>Artist:</label><input id="track_artist" type="text" required><br>'+
	'<label>Album:</label><input id="track_album" type="text"><br>'+
	'<label>Genre:</label><input id="track_genre" type="text" required><br>'+
	'<label>Composer:</label><input id="track_composer" type="text"><br>';

var playlistCreation = 
	'<label>Playlist Name:</label><input id="playlistName" type="text" required><br>';
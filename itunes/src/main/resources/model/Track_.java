package model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-03-23T17:24:42.299+0000")
@StaticMetamodel(Track.class)
public class Track_ {
	public static volatile SingularAttribute<Track, Long> trackId;
	public static volatile SingularAttribute<Track, String> name;
	public static volatile SingularAttribute<Track, String> artist;
	public static volatile SingularAttribute<Track, String> album;
	public static volatile SingularAttribute<Track, String> genre;
	public static volatile SingularAttribute<Track, String> kind;
	public static volatile SingularAttribute<Track, String> composer;
	public static volatile SingularAttribute<Track, Long> size;
	public static volatile SingularAttribute<Track, Long> totalTime;
	public static volatile SingularAttribute<Track, Date> dateAdded;
	public static volatile SingularAttribute<Track, Date> dateModified;
	public static volatile SingularAttribute<Track, Integer> bitRate;
	public static volatile SingularAttribute<Track, Integer> sampleRate;
	public static volatile SingularAttribute<Track, String> persistentId;
	public static volatile SingularAttribute<Track, Integer> playCount;
	public static volatile SingularAttribute<Track, Date> playDate;
	public static volatile SingularAttribute<Track, Boolean> hasVideo;
	public static volatile SingularAttribute<Track, Boolean> hd;
	public static volatile SingularAttribute<Track, Integer> videoWidth;
	public static volatile SingularAttribute<Track, Integer> videoHeight;
	public static volatile SingularAttribute<Track, User> user;
}

package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-03-26T00:09:22.418+0000")
@StaticMetamodel(PlaylistTrack.class)
public class PlaylistTrack_ {
	public static volatile SingularAttribute<PlaylistTrack, Integer> id;
	public static volatile SingularAttribute<PlaylistTrack, Crew> crew;
}

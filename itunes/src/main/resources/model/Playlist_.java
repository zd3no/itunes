package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-03-23T17:24:42.264+0000")
@StaticMetamodel(Playlist.class)
public class Playlist_ {
	public static volatile SingularAttribute<Playlist, Integer> id;
	public static volatile SingularAttribute<Playlist, String> persistentId;
	public static volatile SingularAttribute<Playlist, String> name;
	public static volatile CollectionAttribute<Playlist, Track> tracks;
	public static volatile SingularAttribute<Playlist, User> user;
}
